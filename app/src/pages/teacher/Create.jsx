import React, { useState } from 'react'
import Axios from 'axios'
import { useHistory } from 'react-router'

const Create = () => {
  const history = useHistory()
  const [formData, setformData] = useState({})
  const [message, setmessage] = useState('')
  const handleSubmit = (e) => {
    e.preventDefault()
    console.log('form submit')
    Axios.post('http://localhost:3000/api/teachers', formData).then((res) => {
      setmessage('Data saved')
      setTimeout(() => {
        history.push('/teachers')
      }, 2000)
      return res.data
    })
  }
  const handleChange = (e) => {
    const fieldName = e.target.name
    const fieldValue = e.target.value
    setformData({
      ...formData,
      [fieldName]: fieldValue
    })
    console.log(formData)
  }
  return (
    <div>
      <form onSubmit={(e) => handleSubmit(e)}>
        <div>
          <input type="text" name="name" onChange={(e) => handleChange(e)}/>
        </div>
        <div>
          <button type="submit">Submit</button>
        </div>
        {message}
      </form>
    </div>
  )
}

export default Create
