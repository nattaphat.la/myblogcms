import React, { useEffect, useState } from 'react'
import BlogList from './components/BlogList'
import Axios from 'axios'
import { Link } from 'react-router-dom'

// const data = [
//   {
//     id: 1,
//     name: 'Blog 01',
//     createdDate: new Date()
//   },
//   {
//     id: 2,
//     name: 'Blog 02',
//     createdDate: new Date()
//   },
//   {
//     id: 3,
//     name: 'Blog 03',
//     createdDate: new Date()
//   }
// ]

const List = () => {
  const [data, setdata] = useState([])
  const [filterFormData, setfilterFormData] = useState({})
  const {
    name
  } = filterFormData
  useEffect(() => {
    let filter = {}
    if (name) {
      filter = {
        where: {
          name: {
            ilike: name
          }
        }
      }
    }
    Axios.get('http://localhost:3000/api/BlogPosts', {
      params: {
        filter,
      }
    }).then(res => {
      setdata(res.data)
      return res.data
    })
  }, [name])
  const handleChange = (e) => {
    const fieldName = e.target.name
    const fieldValue = e.target.value
    setfilterFormData({
      ...filterFormData,
      [fieldName]: fieldValue
    })
  }
  const handleSearch = (e) => {
    e.preventDefault()
    // set filter
  }
  return (
    <div>
      <Link to="/blogs/create">Create</Link>
      <div>
        <form onSubmit={(e) => handleSearch(e)}>
          <input type="text" placeholder="search" name="name" onChange={handleChange}/>
        </form>
      </div>
      <BlogList data={data}/>
    </div>
  )
}

export default List
