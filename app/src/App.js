import React from 'react';
import './App.css';
import { BrowserRouter, Link, Switch, Route } from 'react-router-dom';
import Home from './pages/home/Home';
import List from './pages/blog/List';
import Create from './pages/blog/Create';
import ListTeacher from './pages/teacher/List';
import CreateTeacher from './pages/teacher/Create';

function App() {
  return (
    <BrowserRouter>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/blogs">Blog</Link>
            </li>
            <li>
              <Link to="/teachers">Teacher</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/blogs" exact>
            <List />
          </Route>
          <Route path="/blogs/create">
            <Create />
          </Route>
          <Route path="/teachers" exact>
            <ListTeacher />
          </Route>
          <Route path="/teachers/create">
            <CreateTeacher />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
